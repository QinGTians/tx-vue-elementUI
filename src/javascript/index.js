

new Vue({
    el: '#app',
    data: function () {
        return {
            // 用户信息
            userInfo : {
                name : '',
                gender : '',
                phoneNum : '',
                birthday : ''
            },
            tableData: [
                {
                name: '唐三',
                gender: '男',
                phoneNum: '13838388888',
                birthday:'1993-10-20'
            }, {
                    name: '小舞',
                    gender: '女',
                    phoneNum: '13838389999',
                    birthday:'1994-10-21'
                }, {
                    name: '王小刚',
                    gender: '男',
                    phoneNum: '13838387777',
                    birthday:'1983-10-22'
                }, {
                    name: '柳二龙',
                    gender: '女',
                    phoneNum: '13838386666',
                    birthday:'1984-10-23'
                }
            ],
            // 弹框的显示
            dialogVisible: false,
            // 弹框的数据类
            editObj: {
                name : '',
                gender : '',
                phoneNum : '',
                birthday : ''
            },
            userIndex: 0
        }
    },
    methods: {
        // 添加用户
        addUser(){
            //校验数据
            if (!this.userInfo.name) {
                this.$message({
                    message: '请输入姓名！',
                    type: 'warning'
                });
                return;
            }
            if (!this.userInfo.gender) {
                this.$message({
                    message: '请输入性别！',
                    type: 'warning'
                });
                return;
            }
            //正则校验
            if (!/^1[3456789]\d{9}$/.test(this.userInfo.phoneNum)) {
                this.$message({
                    message: '请输入正确的电话号码！',
                    type: 'warning'
                });
                return;
            }
            if (!this.userInfo.birthday) {
                this.$message({
                    message: '请输入生日！',
                    type: 'warning'
                });
                return;
            }
            //保存数据
         this.tableData.push(this.userInfo);
         //添加完后清空数据
            this.userInfo = {
                name : '',
                gender : '',
                phoneNum : '',
                birthday : ''
            };
        },
        // 删除一组数据
        delUser(index){
            this.$confirm('确认删除？')
                .then(_ => {
                    this.tableData.splice(index,1);
                })
                .catch(_ => {});
        },
        //编辑数据
        editUser(item,idx){
            //确定是哪条数据
            this.userIndex = idx;
            this.editObj = {
                name : item.name,
                gender : item.gender,
                phoneNum : item.phoneNum,
                birthday : item.birthday
            };
            this.dialogVisible = true;
        },
        // 叉掉弹出框
        handleClose(){
            this.dialogVisible = false;
        },
        //确定编辑事件
        confirm(){
            this.dialogVisible = false;
            //渲染页面
            // Vue.set(vm.items, indexOfItem,newValue);
            Vue.set(this.tableData, this.userIndex,this.editObj);
        }


    }

});