# tx-vue-elementUI

#### 介绍
cdn引入的vue,elementui，非npm,实现基本增删改
简单的校验

#### 软件架构
引入了vue.js,elementui.js/css


#### 安装教程
webstorm打开直接跑index.html

#### 使用说明
![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/103312_7d9369bc_766177.png "234.png")




#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
